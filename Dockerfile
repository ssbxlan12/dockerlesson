FROM python:3-alpine

EXPOSE 8000
#ALLOWED_HOSTS=64.225.67.204,127.0.0.1
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY . /code/
RUN pip3 install -r requirements.txt
CMD /bin/sh init.sh && python3 manage.py runserver 0.0.0.0:8000
